package app.demo.user.service;

import core.framework.util.Randoms;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rickey
 */
public class LearnCoreNGService {
    
    private static final Logger logger = LoggerFactory.getLogger(LearnCoreNGService.class);
    
    public void pacticeUtility() {
        char ch = 'h';
        logger.info("'{}' is lower case ? {}", ch, core.framework.util.ASCII.isLowerCase('h'));
        logger.info("random num: {}", Randoms.nextInt(0, 1000));
        
        String text1 = "str1";
        String text2 = "str2";
        logger.info("\"{}\" equal \"{}\" ? {}", text1, text2, Strings.equals(text1, text2));
    }
}
