package app;

import app.demo.user.service.LearnCoreNGService;
import core.framework.module.Module;

/**
 * @author Rickey
 */
public class UserModule extends Module {
    
    @Override
    protected void initialize() {
        LearnCoreNGService learnCoreNGService = bind(LearnCoreNGService.class);
        learnCoreNGService.pacticeUtility();
    }
}
