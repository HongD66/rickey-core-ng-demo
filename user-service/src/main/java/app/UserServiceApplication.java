package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Rickey
 */
public class UserServiceApplication extends App {
    
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        load(new UserModule());
    }
}