import app.UserServiceApplication;

/**
 * @author Rickey
 */
public class Main {
    
    public static void main(String[] args) {
        new UserServiceApplication().start();
    }
}
